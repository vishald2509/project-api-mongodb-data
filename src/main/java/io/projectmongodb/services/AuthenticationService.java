package io.projectmongodb.services;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import io.projectmongodb.dto.SigninRequest;
import io.projectmongodb.dto.SignupRequest;
import io.projectmongodb.exception.ProjectmongodbException;
import io.projectmongodb.model.TokenAuthentication;
import io.projectmongodb.model.User;
import io.projectmongodb.repository.TokenAuthenticationRepository;
import io.projectmongodb.repository.UserRepository;
import io.projectmongodb.utils.ProjectmongoConstants.MessageText;

@Service
public class AuthenticationService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private TokenAuthenticationRepository tokenAuthenticationRepository;

	public String addUser(SignupRequest signupRequest) {
		String id = UUID.randomUUID().toString(); // Gennerate UUID
		User user = new User(id, signupRequest.getUsername(), signupRequest.getEmail(), signupRequest.getPassword(),
				signupRequest.getUsertype(), signupRequest.getPhoneno());
		userRepository.save(user);
		return MessageText.SUCCESFUL;
	}

	public String authenticateUser(SigninRequest signinRequest) {

		User user = userRepository.findByUsername(signinRequest.getUsername());

		if (user.equals(null)) {
			return MessageText.INVALID_USER;
		}
		if (user.getPassword().toString().equals(signinRequest.getPassword().toString())) {
			String id = UUID.randomUUID().toString();
			String token = UUID.randomUUID().toString();
			Integer usertype = user.getUsertype();
			TokenAuthentication tokenAuthentication = new TokenAuthentication(id, token, usertype, user);
			tokenAuthenticationRepository.save(tokenAuthentication);
			return MessageText.SUCCESFUL;
		} else {
			return MessageText.INVALID_PASSWORD;
		}

	}

	public ResponseEntity<Object> userSignOut(String token) throws ProjectmongodbException {
		// TODO Auto-generated method stub
		if (token == null) {
			throw new ProjectmongodbException("Invalid Token");
		}
		tokenAuthenticationRepository.deleteByToken(token);
		return new ResponseEntity<Object>(MessageText.SUCCESFUL, HttpStatus.OK);
	}

}

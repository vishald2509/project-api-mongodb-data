package io.projectmongodb.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;

@Document(collection = "user") @Data @AllArgsConstructor
public class User {
	
	@Id
	private String id;
	private String username;
	private String email;
	private String password;
	private int usertype;
	private long phoneno;
	
	
}

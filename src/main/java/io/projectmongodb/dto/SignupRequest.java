package io.projectmongodb.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SignupRequest {
	private String username;
	private String email;
	private String password;
	private int usertype;
	private long phoneno;
}

package io.projectmongodb.utils;

public class ProjectmongoConstants {
	public class AccountType{
		public static final int ADMIN = 1;
		public static final int USER = 2;
		public static final int VENDOR = 3;
	}
	
	public class MessageText{
		public static final String SUCCESFUL = "Succesful";
		public static final String INVALID_PASSWORD = "PASSWORD is Invalid";
		public static final String INVALID_USER = "USER is Invalid";
	}
}

package io.projectmongodb.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.projectmongodb.dto.ResponseDTO;
import io.projectmongodb.dto.SigninRequest;
import io.projectmongodb.dto.SignupRequest;
import io.projectmongodb.exception.ProjectmongodbException;
import io.projectmongodb.services.AuthenticationService;

@RestController
@RequestMapping("/account")
public class AccountController {

	@Autowired
	private AuthenticationService authenticationService;

	// TODO SignUp
	@PostMapping(path = "/signup")
	public String userSignup(@RequestBody SignupRequest signupRequest) {
		return authenticationService.addUser(signupRequest);
	}

	// TODO SignIn
	@PostMapping(path = "/signin")
	public ResponseDTO userSignIn(@RequestBody SigninRequest signinRequest) throws ProjectmongodbException {
		
		if (signinRequest.getUsername() == null) {
			throw new ProjectmongodbException("Username Not Found");
			//return
		}
		
		return new ResponseDTO(authenticationService.authenticateUser(signinRequest));
	}

	// TODO SignOut
	@DeleteMapping(path = "/signout")
	public ResponseEntity<Object> userSignOut(@RequestParam(value = "token") String token) throws ProjectmongodbException {
		ResponseEntity<Object> response = authenticationService.userSignOut(token);
		return response;
	}

	// TODO Access Account Details("/details") [username, phonenumber, emailid]

	// TODO Update Account Details("/updatedetails") [username, phonenumber,
	// emailid]

	// TODO Update Password ("/updatepassword")

}
